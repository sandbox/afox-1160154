<?php
// $Id: bt_txn_type.views.inc
 
 /**
 * Implementation of hook_views_data_alter()
 */
function bt_txn_type_views_data(){

  $data['bt_txn_type']['table']['group']  = t('Balances');

  $data['bt_txn_type']['table']['base'] = array(
    'field' => 'txn_type',
    'title' => t('Balance Tracker transaction type'),
    'help' => t('Transaction types for Balance Tracker transactions.'),
  );
  $data['bt_txn_type']['table']['join'] = array(
    'balance_items' => array(
      'left_field' => 'txn_type',
      'field' => 'txn_type',
    ),
  );
  $data['bt_txn_type']['txn_type'] = array(
    'title' => t('Transaction type'),
    'help' => t('Balance Tracker Transaction type ID'),
		'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  $data['bt_txn_type']['txn_name'] = array(
    'title' => t('Transaction name'),
    'help' => t('Balance Tracker Transaction type name'),
		'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_text',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}

/*
 function bt_txn_type_views_data_alter(&$data) {
  $data['balance_items']['txn_type'] = array(
    'title' => t('Transaction type'),
    'help' => t('Balance Tracker Transaction type ID'),
		'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  return $data;
}
*/